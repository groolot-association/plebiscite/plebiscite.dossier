byte valArray[4];

void setup() {
  Serial.begin(4800);
}

void loop() {
  valArray[0] = (analogRead(1) >> 3) + 128;
  valArray[1] = (analogRead(3) >> 2) + 128;
  valArray[2] = (analogRead(4) >> 3) + 128;
  valArray[3] = 0;
  Serial.write(valArray, 4);
  delay(7);
}
